const {ccclass, property} = cc._decorator;

@ccclass
export default class Laba extends cc.Component {

    @property([cc.Node])
    items: cc.Node[] = [];

    /**
     * 想象里面的滚动时由个锤子往下拉 这是拉的高度，可根据滚多少圈，到那一项来计算
     */
    height: number = 0;

    /**是否正在滚动中 */
    doRuning: number = 0;

    /**
     * 当前转了多少时间
     */
    runTime: number = 0;

    /**
     * 转多长时间
     */
    runEndTime: number = 5;

    /**
     * 当前拉了多长
     */
    moveLen: number = 0;

    prevMove: number = 0;

    // Rolling the wheel with position.y + 100
    resetItemPos() {
        let pos = 0;
        for(let i = this.items.length - 1; i >= 0; i--) {
            // console.log("here is i "+i);
            this.items[i].y = pos;
            // console.log("Items here "+this.items[i].y)
            pos += this.height;
            // console.log("Total==="+pos);
        }
    }

    doStart() {
        this.resetItemPos();
        // k 是随机数得到的奖励结果
        let k = Math.floor(Math.random() * 100);
        let res = k % this.items.length;
        //最底部奖励1 然后往上2。。。。
        console.log("随机为奖励" + (res + 1))
        this.runTime = 0;
        console.log("k is here == "+k)
        this.moveLen = k * this.height;
        console.log("Move Length Here === "+this.moveLen)
        this.prevMove = 0;
        this.doRuning = 1;
    }

    // Excute moving down 
    moving(dt:number) {
        let moveDelta = this.moveDown(dt);
        console.log("Move Down DT here "+moveDelta);
        // 往下滑动
        for(let i = 0; i < this.items.length; i++) {
            let n = this.items[i];
            n.y -= moveDelta; // << 往下减
        }
        // 判断把最底部的 从最高位置生成
        for(let i = 0; i < this.items.length; i++) {
            let n = this.items[i];
            // 判定array里的 object 是否小于 （-1*this.height）
            if(n.y < -2.5 * this.height) {
                // 判断生成 
                let k = (i + 1) >= this.items.length ? 0 : i + 1;  // condition if else ShortCut
                n.y = this.items[k].y + this.height;
            }
        }
    }

    // 尝试用自己的 方式 更改
    moveDown(dt: number) {
        // how to get a random number to count Down?? <<<<<<<<<<<<<<<<<<<<<<<<
        this.runTime += dt;
        let timer = this.runTime / this.runEndTime; 
        timer -= 1;
        // timer =  timer * timer * timer  + 1; //越来越慢的计算法
        timer =  timer * timer * timer  + 1; 
        let currMove = timer * this.moveLen;
        let moveDelta = currMove - this.prevMove;
        this.prevMove = currMove;
        // STOP the wheeling when it is countDown (this.runTime) == 0
        if(this.runTime >= this.runEndTime) {
            this.doRuning = 0;
        }
        return moveDelta;
    }
    //=======================================

    // Calculation for moving down =========================================
    // moveDown(dt: number) {
    //     this.runTime += dt;
    //     let timer = this.runTime / this.runEndTime;
    //     timer -= 1;
    //     timer =  timer * timer * timer  + 1;
    //     let currMove = timer * this.moveLen;
    //     let moveDelta = currMove - this.prevMove;
    //     this.prevMove = currMove;
    //     // STOP the wheeling when it is countDown (this.runTime) == 0
    //     if(this.runTime >= this.runEndTime) {
    //         this.doRuning = 0;
    //     }
    //     return moveDelta;
    // }


    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        this.height = 100;
    }

    update (dt) {
        console.log("calling update...")
        if(this.doRuning == 1) {
            this.moving(dt);
        }
    }
}
