cc.Class({
    extends: cc.Component,

    properties: {
        rewards:{
            default: [],
            type: cc.Node
        },

        buttonStart:{
            default: null,
            type: cc.Button
        },

        animationLeds:{
            default: null,
            type: cc.Animation
        },

        runTime: 0,
        height: 0,
        rewardsNum: 8,
        moveLen: 0,
        prevMove: 0,
        // rollTime: 0,
        endTime: 5,
        roll: false,

        result:{
            default: null,
            type: cc.Label
        },
    },

    onLoad () {

    },

    start () {
        this.height = 100;
    },

    buttonOnClick(){
        this.resetItemPos();
        this.animationLeds.play('Rolling');
        let k = (Math.floor(Math.random()*100));
        var finalGet = this.rollingRate(k);
        let resultRoll = finalGet % this.rewards.length; 
        console.log("REWARD IS ... "+resultRoll +" k is "+k );
        this.result.string = "Rolling..."
        this.runTime = 0;
        this.moveLen = finalGet * this.height;
        this.prevMove = 0;
        this.roll = true;
        this.buttonStart.enabled  = false;

        // 延迟显示
        var node = cc.find('Canvas/result')
            cc.tween(node)
                .to(this.endTime)
                .call(()=> {this.showResult(resultRoll+1)})
                .call(()=> {this.roll = false})
                .call(()=> {this.buttonStart.enabled  = true})
                .call(()=> {this.animationLeds.play('ledrunning');})
                .start()
    },

    update (dt) {
        if(this.roll == true){

            this.moving(dt);
        }
    },

    moving:function(dt){

        let moveDelta = this.moveDown(dt);
        //一个个往下转
        for(let i = 0; i < this.rewards.length; i++) {
            let n = this.rewards[i];
            n.y -= moveDelta;
        }
        // 往下转到了 y(-2.5*100) 的格子自动消失
        for(let i = 0; i < this.rewards.length; i++) {
            let n = this.rewards[i];
            if(n.y < -2.5 * this.height) {
                let k = (i + 1) >= this.rewards.length ? 0 : i + 1;
                n.y = this.rewards[k].y + this.height;
            }
        }
    },

    // 往下转的计算法
    moveDown: function(dt){
        this.runTime += dt;
        let timer = this.runTime / this.endTime;
        timer -= 1; 
        timer =  timer * timer * timer  + 1;
        let currMove = timer * this.moveLen;
        let moveDelta = currMove - this.prevMove;
        this.prevMove = currMove;
        // STOP the wheeling when it is countDown (this.runTime) == 0
        if(this.runTime >= this.endTime) {
            this.roll = false;
        }
        return moveDelta;
    },

    // 恢复原来的位置
    resetItemPos(){
        let pos = 0;
        for(let i = this.rewards.length - 1; i >= 0; i--){
            this.rewards[i].y = pos;
            pos += this.height;
        }
    },

    showResult(res){
        switch(res) {
            case 8:
            console.log(8);
            this.result.string = "100 砖石福袋";
            break;
            case 7:
            console.log(7);
            this.result.string  = "皮蛋豆腐";
            break;
            case 6:
            console.log(6);
            this.result.string  = "10元 欢喜福袋";
            break;
            case 5:
            console.log(5);
            this.result.string  = "皮蛋豆腐";
            break;
            case 4:
            console.log(4);
            this.result.string  = "50 金银福袋";
            break;
            case 3:
            console.log(3);
            this.result.string  = "10元 欢喜福袋";
            break;
            case 2:
            console.log(2);
            this.result.string  = "皮蛋豆腐";
            break;
            case 1:
            console.log(1);
            this.result.string  = "10元 欢喜福袋";
            break;
        }
    },

    // Here is that number for % with this.rewards.length 
    //96=0(1)  97=1(2) 90=2(3) ,91=3(4) 92=4(5) 93=5(6) 94=6(7) 95=7(8)
    // 752 == 24%
    // 631 == 7%
    // 8 == 2%
    // 4 == 5%

    //this is adjust the rate for reward output
    rollingRate(rollNumber)
    {
        // 2% output
        if(rollNumber <= 2)
        {
            return 95;
        }

        // 5%
        else if(rollNumber <= 7)
        {
            return 91;
        }

        // 7%
        else if(rollNumber <= 14)
        {
            return 96;
        }

        // 7%
        else if(rollNumber <= 21)
        {
            return 90;
        }

        // 7%
        else if(rollNumber <= 28)
        {
            return 93;
        }

        // 24%
        else if(rollNumber <= 52)
        {
            return 94;
        }

        // 24%
        else if(rollNumber <= 76)
        {
            return 92;
        }

        // 24%
        else
        {
            return 97;
        }
    },
});
